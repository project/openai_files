# OpenAI Files
Enables files to be added to an AI (Artificial Intelligence) which can then be
used as the source of knowledge for replies.

Although this module can be used on it's own to manage files added to an AI
service its main use is to provide that functionality to other AI modules.

Currently limited to using the OpenAI API service, for which a paid
subscription is required. Using alternative AI services is on the
development roadmap, including self hosted AI.

For a full description of the module, visit the [project page](https://www.drupal.org/project/openai_files).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/openai_files).


## Requirements

This module requires the following modules:

- [OpenAI](https://www.drupal.org/project/openai)

An [OpenAI API](https://openai.com/pricing) subscription is required.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module: Administration > Extend.

Use the "AI Data" Media entity through an entity reference field.


## Maintainers

- Robert Castelo - [robert-castelo](https://www.drupal.org/u/robert-castelo)
